if (\basename -- /) >/dev/null 2>&1 && \test "X`\basename -- / 2>&1`" = "X/"; then
  as_basename=basename
else
  as_basename=false
fi
echo as_basename:$as_basename
